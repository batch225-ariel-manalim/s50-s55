import {Link} from 'react-router-dom'

export default function Error(){
	return (
		<>
			<h1>Page Not Found</h1>
			<p>Go back to <Link to="/"> homepage</Link>.</p>
		</>
	)
}
